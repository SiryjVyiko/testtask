FROM centos:latest

ADD nginx.repo /etc/yum.repos.d/nginx.repo
ARG login='user1' 
ARG password='password1'

RUN dnf -y install net-tools httpd-tools nginx && \
    mkdir -p /usr/share/nginx/html/auth && \
    htpasswd -bc /etc/nginx/.htpasswd $login $password

ADD src/. /

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]